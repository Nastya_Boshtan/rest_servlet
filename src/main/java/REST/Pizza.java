package REST;

public class Pizza {
    private String name;
    private int id;
    private static int unique=1;

    public Pizza(String name){
        this.name=name;
        id=unique;
        unique++;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id +". "+ name;

    }


}
