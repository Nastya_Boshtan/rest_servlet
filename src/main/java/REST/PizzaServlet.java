package REST;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


@WebServlet(urlPatterns = "/pizza/*")
public class PizzaServlet  extends HttpServlet {
    private static Map<Integer,Pizza> pizza = new HashMap<>();
    private Logger logger = LogManager.getLogger(PizzaServlet.class);
    @Override
    public void init() throws ServletException {
        Pizza pizza1=new Pizza("Palerma");
        Pizza pizza2 = new Pizza("Havayska");
        pizza.put(pizza1.getId(),pizza1);
        pizza.put(pizza2.getId(),pizza2);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<p> <a href='pizza'>REFRESH</a> </p>");
        out.println("<h2>Orders</h2>");
        for(Pizza pizzas: pizza.values()){
            out.println("<p>"+pizzas+"</p>");
        }
        out.println("<form action='pizza' method='POST'>\n"
                + "  Name of Pizza: <input type='text' name='pizza_name'>\n"
                + "  <button type='submit'>Save Order</button>\n"
                + "</form>");

        out.println("<form>\n"
                + "  <p><b>DELETE ELEMENT</b></p>\n"
                + "  <p> Pizza id: <input type='text' name='pizza_id'>\n"
                + "    <input type='button' onclick='remove(this.form.pizza_id.value)' name='ok' value='Delete pizza'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('pizza/' + id, {method: 'DELETE'}); }\n"
                + "</script>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newPizza = req.getParameter("pizza_name");
        Pizza newOrder = new Pizza(newPizza);
        pizza.put(newOrder.getId(),newOrder);
        doGet(req,resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getRequestURI();
        id=id.replace("/Servlet-1.0-SNAPSHOT/pizza/","");
        pizza.remove(Integer.parseInt(id));
    }
























}
